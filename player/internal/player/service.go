package player

import (
	"bytes"
	"context"
	"encoding/csv"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"math/rand"
	"net/http"
	"os"
	"pingpong-go-player/internal"
	"pingpong-go-player/internal/models"
	"runtime"
	"strconv"
	"strings"
	"sync"
	"time"

	"github.com/go-redis/redis/v8"
)

type MatchRepo interface {
	AddMatchLog(ctx context.Context, matchLogs []models.Match) error
	GetMatchById(ctx context.Context, matchId string) ([]models.Match, error)
}

type Service struct {
	cv   *internal.Configs
	repo MatchRepo
}

func NewService(cv *internal.Configs) *Service {
	return &Service{
		cv:   cv,
		repo: NewRepo(cv),
	}
}

func (s Service) NewMatch(c context.Context) error {
	var wg sync.WaitGroup
	var matchLogs []models.Match
	var matchId int

	redisClient := redis.NewClient(&redis.Options{
		Addr:     "localhost:6379",
		Password: "MoUpg0PPoN",
		DB:       0, // use default DB
	})

	lastMatchIdString, err := redisClient.Get(c, "match_id").Result()
	if err == redis.Nil {
		matchId = 1
	} else if err != nil {
		log.Fatal(err)
	} else {
		lastMatchId, err := strconv.Atoi(lastMatchIdString)
		if err != nil {
			log.Fatal(err)
		}
		matchId = lastMatchId + 1
	}

	matchChannel := make(chan models.Match)
	defer close(matchChannel)
	logChannel := make(chan models.Match)
	defer close(logChannel)

	wg.Add(3)
	go Player("A", true, matchChannel, logChannel, &wg)
	go Player("B", false, matchChannel, logChannel, &wg)

	go WriteLog(matchId, logChannel, &wg, &matchLogs)
	wg.Wait()

	err = s.repo.AddMatchLog(c, matchLogs)

	if err != nil {
		log.Fatal(err)
	}

	p, err := json.Marshal(matchLogs)
	if err != nil {
		return err
	}

	err = redisClient.Set(c, "match_id", matchId, 0).Err()
	if err != nil {
		return err
	}

	err = redisClient.Set(c, "match", p, 0).Err()
	if err != nil {
		return err
	}

	return nil
}

func (s Service) GetLastMatch(c context.Context) ([]models.Match, error) {
	var matchLogs []models.Match
	redisClient := redis.NewClient(&redis.Options{
		Addr:     "localhost:6379",
		Password: "MoUpg0PPoN",
		DB:       0, // use default DB
	})

	matchJSON, err := redisClient.Get(c, "match").Result()

	if err == redis.Nil {
		return nil, err
	} else if err != nil {
		log.Fatal(err)
	}

	err = json.Unmarshal([]byte(matchJSON), &matchLogs)
	if err != nil {
		log.Fatal(err)
	}

	return matchLogs, err

}

func (s Service) GetMatchById(c context.Context, matchId string) ([]models.Match, error) {
	data, err := s.repo.GetMatchById(c, matchId)

	fmt.Println(data)
	if err != nil {
		log.Fatal(err)
	}

	return data, err
}

func Player(playerName string, isServe bool, matchChannel chan models.Match, logChannel chan models.Match, wg *sync.WaitGroup) {
	defer wg.Done()

	if isServe {
		servedBallPower := Serve()
		fmt.Printf("Serve %s: %f\n", playerName, servedBallPower)

		m := models.Match{
			Turn:             1,
			WakingProcess:    playerName,
			BallPowerHit:     servedBallPower,
			BallPowerReceive: 0,
			GoRoutineNumber:  goid(),
			Time:             time.Now(),
		}
		matchChannel <- m
		logChannel <- m
	}

	for {
		rm := <-matchChannel
		returnedBallPower := Table(rm.BallPowerHit)
		fmt.Printf("Return %s: %f\n", playerName, returnedBallPower)

		if rm.IsEndMatch {
			fmt.Printf("%s Win\n", playerName)
			break
		}

		servedBallPower := Serve()
		fmt.Printf("Serve %s: %f\n", playerName, servedBallPower)

		if servedBallPower <= returnedBallPower {
			fmt.Printf("%s Lose\n", playerName)
			m := models.Match{
				Turn:             rm.Turn + 1,
				WakingProcess:    playerName,
				BallPowerReceive: returnedBallPower,
				BallPowerHit:     servedBallPower,
				GoRoutineNumber:  goid(),
				Time:             time.Now(),
				IsEndMatch:       true,
			}
			matchChannel <- m
			logChannel <- m
			break
		}

		m := models.Match{
			Turn:             rm.Turn + 1,
			WakingProcess:    playerName,
			BallPowerReceive: returnedBallPower,
			BallPowerHit:     servedBallPower,
			GoRoutineNumber:  goid(),
			Time:             time.Now(),
		}
		matchChannel <- m
		logChannel <- m
	}

	fmt.Printf("%s End\n", playerName)
}

func WriteLog(matchId int, logChannel chan models.Match, wg *sync.WaitGroup, matchLogs *[]models.Match) {
	defer wg.Done()

	f, err := os.Create("users.csv")

	if err != nil {
		log.Fatalln("failed to open file", err)
	}
	defer f.Close()

	for v := range logChannel {
		v.MatchNumber = matchId
		*matchLogs = append(*matchLogs, v)
		fmt.Println(v)

		w := csv.NewWriter(f)
		err = w.Write([]string{
			fmt.Sprintf("%d", v.MatchNumber),
			fmt.Sprintf("%d", v.Turn),
			v.WakingProcess,
			fmt.Sprintf("%f", v.BallPowerReceive),
			fmt.Sprintf("%f", v.BallPowerHit),
			fmt.Sprintf("%d", v.GoRoutineNumber),
			v.Time.Format("2006-01-02T15:04:05.000"),
		})

		if err != nil {
			log.Fatal(err)
		}
		w.Flush()

		if v.IsEndMatch {
			break
		}
	}

}

func Serve() float64 {
	return rand.Float64() * 100
}

func Table(ballPower float64) float64 {
	var request struct {
		BallPower float64 `json:"ball_power"`
	}

	var response struct {
		Data float64 `json:"data"`
	}

	request.BallPower = ballPower

	p, err := json.Marshal(request)
	if err != nil {
		log.Fatalf("An Error Occured %v", err)
	}

	resp, err := http.Post("http://localhost:8889/table", "application/json", bytes.NewBuffer(p))
	if err != nil {
		log.Fatalf("An Error Occured %v", err)
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Fatalln(err)
	}

	err = json.Unmarshal(body, &response)
	if err != nil {
		panic(err.Error())
	}

	return response.Data
}

func goid() int {
	var buf [64]byte
	n := runtime.Stack(buf[:], false)
	idField := strings.Fields(strings.TrimPrefix(string(buf[:n]), "goroutine "))[0]
	id, err := strconv.Atoi(idField)
	if err != nil {
		panic(fmt.Sprintf("cannot get goroutine id: %v", err))
	}
	return id
}
