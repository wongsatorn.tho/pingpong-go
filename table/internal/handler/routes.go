package handler

import (
	"net/http"
	"pingpong-go-table/internal"
	"pingpong-go-table/internal/table"

	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
)

type CloseFunc func() error

type route struct {
	Group          string
	Path           string
	HttpMethod     string
	HandlerFunc    echo.HandlerFunc
	MiddlewareFunc []echo.MiddlewareFunc
}

func NewRoutes(e *echo.Echo, cv *internal.Configs) ([]CloseFunc, error) {
	closeFuncs := make([]CloseFunc, 0)

	tb := table.NewEndpoint(cv)

	routes := []route{
		{
			Group:          "",
			Path:           "/table",
			HttpMethod:     http.MethodPost,
			HandlerFunc:    tb.Table,
			MiddlewareFunc: nil,
		},
	}

	// http connection
	for _, rt := range routes {
		mw := []echo.MiddlewareFunc{
			middleware.BodyDumpWithConfig(BodyDumpConfig()),
		}
		mw = append(mw, rt.MiddlewareFunc...)
		e.Group(rt.Group).Add(rt.HttpMethod, rt.Path, rt.HandlerFunc, mw...)
	}

	return closeFuncs, nil
}
