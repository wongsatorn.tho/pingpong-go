package handler

import (
	"net/http"
	"pingpong-go-player/internal"
	"pingpong-go-player/internal/health"
	"pingpong-go-player/internal/player"

	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
)

type CloseFunc func() error

type route struct {
	Group          string
	Path           string
	HttpMethod     string
	HandlerFunc    echo.HandlerFunc
	MiddlewareFunc []echo.MiddlewareFunc
}

func NewRoutes(e *echo.Echo, cv *internal.Configs) ([]CloseFunc, error) {
	closeFuncs := make([]CloseFunc, 0)

	pl := player.NewEndpoint(cv)

	routes := []route{
		{
			Group:          "",
			Path:           "/health_check",
			HttpMethod:     http.MethodGet,
			HandlerFunc:    health.HealthCheck,
			MiddlewareFunc: nil,
		},
		{
			Group:          "",
			Path:           "/new_match",
			HttpMethod:     http.MethodGet,
			HandlerFunc:    pl.NewMatch,
			MiddlewareFunc: nil,
		},
		{
			Group:          "",
			Path:           "/match/:match_id",
			HttpMethod:     http.MethodGet,
			HandlerFunc:    pl.GetMatchById,
			MiddlewareFunc: nil,
		},
		{
			Group:          "",
			Path:           "/match",
			HttpMethod:     http.MethodGet,
			HandlerFunc:    pl.GetLastMatch,
			MiddlewareFunc: nil,
		},
	}

	// http connection
	for _, rt := range routes {
		mw := []echo.MiddlewareFunc{
			middleware.BodyDumpWithConfig(BodyDumpConfig()),
		}
		mw = append(mw, rt.MiddlewareFunc...)
		e.Group(rt.Group).Add(rt.HttpMethod, rt.Path, rt.HandlerFunc, mw...)
	}

	return closeFuncs, nil
}
