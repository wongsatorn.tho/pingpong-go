package player

import (
	"context"
	"net/http"
	"pingpong-go-player/internal"
	"pingpong-go-player/internal/models"

	"github.com/labstack/echo/v4"
)

type playerService interface {
	NewMatch(ctx context.Context) error
	GetLastMatch(ctx context.Context) ([]models.Match, error)
	GetMatchById(ctx context.Context, matchId string) ([]models.Match, error)
}

type Endpoint struct {
	cv  *internal.Configs
	srv playerService //Service Interface Tier
}

func NewEndpoint(cv *internal.Configs) *Endpoint {
	return &Endpoint{cv: cv, srv: NewService(cv)}
}

func (e Endpoint) NewMatch(c echo.Context) error {

	err := e.srv.NewMatch(c.Request().Context())
	if err != nil {
		return c.JSON(http.StatusBadRequest, models.Response{
			Success: false,
			Code:    http.StatusBadRequest,
			Message: err.Error(),
			Data:    nil,
		})
	}

	return c.JSON(http.StatusOK, models.Response{
		Success: true,
		Code:    http.StatusOK,
		Message: "ok",
		Data:    nil,
	})
}

func (e Endpoint) GetMatchById(c echo.Context) error {
	data, err := e.srv.GetMatchById(c.Request().Context(), c.Param("match_id"))

	if err != nil {
		return c.JSON(http.StatusBadRequest, models.Response{
			Success: false,
			Code:    http.StatusBadRequest,
			Message: err.Error(),
			Data:    nil,
		})
	}

	return c.JSON(http.StatusOK, models.Response{
		Success: true,
		Code:    http.StatusOK,
		Message: "ok",
		Data:    data,
	})
}

func (e Endpoint) GetLastMatch(c echo.Context) error {
	data, err := e.srv.GetLastMatch(c.Request().Context())

	if err != nil {
		return c.JSON(http.StatusBadRequest, models.Response{
			Success: false,
			Code:    http.StatusBadRequest,
			Message: err.Error(),
			Data:    nil,
		})
	}

	return c.JSON(http.StatusOK, models.Response{
		Success: true,
		Code:    http.StatusOK,
		Message: "ok",
		Data:    data,
	})
}
