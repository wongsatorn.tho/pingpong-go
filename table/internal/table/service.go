package table

import (
	"context"
	"math/rand"
	"pingpong-go-table/internal"
)

type Service struct {
	cv *internal.Configs
}

func NewService(cv *internal.Configs) *Service {
	return &Service{
		cv: cv,
	}
}

func (s Service) Table(c context.Context, ballPower float64) (float64, error) {
	tableFactor := (70.0 + rand.Float64()*20) / 100.0
	return ballPower * tableFactor, nil
}
