package player

import (
	"context"
	"fmt"
	"pingpong-go-player/internal"
	"pingpong-go-player/internal/models"
	"strconv"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type Repo struct {
	cv   *internal.Configs
	repo *mongo.Database
}

func NewRepo(cv *internal.Configs) *Repo {
	return &Repo{
		cv:   cv,
		repo: cv.MongoDB.Client.Database(cv.MongoDB.Database),
	}
}

func (r Repo) AddMatchLog(ctx context.Context, matchLogs []models.Match) error {

	coll := r.repo.Collection("match")

	var matchLogsInterface []interface{}

	for _, v := range matchLogs {
		fmt.Println(matchLogsInterface)
		matchLogsInterface = append(matchLogsInterface, v)
	}

	matchResult, err := coll.InsertMany(ctx, matchLogsInterface)

	if err != nil {
		return err
	}

	fmt.Printf("Inserted %v documents into match collection!\n", len(matchResult.InsertedIDs))

	return nil
}

func (r Repo) GetMatchById(ctx context.Context, matchId string) ([]models.Match, error) {

	matchNumber, err := strconv.Atoi(matchId)
	if err != nil {
		return nil, err
	}

	filter := bson.M{
		"match_number": matchNumber,
	}

	coll := r.repo.Collection("match")
	cur, err := coll.Find(ctx, filter, options.Find())
	if err != nil {
		return nil, err
	}

	var rtn []models.Match
	if err := cur.All(ctx, &rtn); err != nil {
		return nil, err
	}
	return rtn, nil
}
