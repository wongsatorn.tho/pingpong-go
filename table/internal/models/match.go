package models

import "time"

type Match struct {
	MatchNumber      int       `bson:"match_number"`
	Turn             int       `bson:"turn"`
	WakingProcess    string    `bson:"waking_process"`
	BallPowerHit     float64   `bson:"ball_power_hit"`
	BallPowerReceive float64   `bson:"ball_power_receive"`
	GoRoutineNumber  int       `bson:"go_routine_number"`
	Time             time.Time `bson:"time"`
	IsEndMatch       bool      `bson:"is_end_match"`
}
