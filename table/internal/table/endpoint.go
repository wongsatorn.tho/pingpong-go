package table

import (
	"context"
	"net/http"
	"pingpong-go-table/internal"
	"pingpong-go-table/internal/models"

	"github.com/labstack/echo/v4"
)

type tableService interface {
	Table(ctx context.Context, ballPower float64) (float64, error)
}

type Endpoint struct {
	cv  *internal.Configs
	srv tableService //Service Interface Tier
}

func NewEndpoint(cv *internal.Configs) *Endpoint {
	return &Endpoint{cv: cv, srv: NewService(cv)}
}

func (e Endpoint) Table(c echo.Context) error {
	var request struct {
		BallPower float64 `json:"ball_power"`
	}

	if err := c.Bind(&request); err != nil {
		return c.JSON(http.StatusBadRequest, models.Response{
			Success: false,
			Code:    http.StatusBadRequest,
			Message: err.Error(),
			Data:    nil,
		})
	}

	data, err := e.srv.Table(c.Request().Context(), request.BallPower)

	if err != nil {
		return c.JSON(http.StatusBadRequest, models.Response{
			Success: false,
			Code:    http.StatusBadRequest,
			Message: err.Error(),
			Data:    nil,
		})
	}

	return c.JSON(http.StatusOK, models.Response{
		Success: true,
		Code:    http.StatusOK,
		Message: "ok",
		Data:    data,
	})
}
